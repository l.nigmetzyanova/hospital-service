package com.technokratos.timetableservice.feign;


import com.technokratos.timetableservice.dto.response.DoctorResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient("logic-service")
public interface DoctorFeignService {

    @GetMapping("/doctor/{id}")
    DoctorResponse getDoctorById(
            @PathVariable UUID id);
}

