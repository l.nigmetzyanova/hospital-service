package com.technokratos.timetableservice.repositories;

import com.technokratos.timetableservice.models.TimetableEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TimetableRepository extends JpaRepository<TimetableEntity, UUID> {
}
