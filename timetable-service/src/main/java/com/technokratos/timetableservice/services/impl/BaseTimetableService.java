package com.technokratos.timetableservice.services.impl;

import com.technokratos.timetableservice.models.TimetableEntity;
import com.technokratos.timetableservice.repositories.TimetableRepository;
import com.technokratos.timetableservice.services.TimetableService;
import com.technokratos.timetableservice.util.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseTimetableService implements TimetableService {

    private final TimetableRepository timetableRepository;

    @Override
    public void createTimetableRecord(TimetableEntity timetableEntity) {
        timetableRepository.save(timetableEntity);
    }

    @Override
    public void deleteById(UUID id) {
        timetableRepository.deleteById(id);
    }

    @Override
    public List<TimetableEntity> getAllTimetable() {
        return timetableRepository.findAll();
    }

    @Override
    public TimetableEntity getTimetableRecordById(UUID id) {
        return timetableRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Timetable record " + id + " not found"));
    }

    @Override
    public void updateTimetableRecordById(UUID id, TimetableEntity timetableEntity) {
        TimetableEntity timetable = timetableRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Timetable record " + id + " not found"));
        timetable.setDoctorId(timetableEntity.getDoctorId());
        timetable.setReceptionId(timetable.getReceptionId());
        timetableRepository.save(timetable);
    }
}
