package com.technokratos.timetableservice.services;

import com.technokratos.timetableservice.models.TimetableEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface TimetableService {

    void createTimetableRecord(TimetableEntity timetableEntity);

    void deleteById(UUID id);

    List<TimetableEntity> getAllTimetable();

    TimetableEntity getTimetableRecordById(UUID id);

    void updateTimetableRecordById(UUID id, TimetableEntity timetableEntity);

}
