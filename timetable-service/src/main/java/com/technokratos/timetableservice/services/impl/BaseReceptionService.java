package com.technokratos.timetableservice.services.impl;


import com.technokratos.timetableservice.models.ReceptionEntity;
import com.technokratos.timetableservice.repositories.ReceptionRepository;
import com.technokratos.timetableservice.services.ReceptionService;
import com.technokratos.timetableservice.util.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class BaseReceptionService implements ReceptionService {

    private final ReceptionRepository receptionRepository;

    @Override
    public void createReception(ReceptionEntity receptionEntity) {
        receptionRepository.save(receptionEntity);
    }

    @Override
    public void deleteById(UUID id) {
        receptionRepository.deleteById(id);
    }

    @Override
    public List<ReceptionEntity> getAllReceptions() {
        return receptionRepository.findAll();
    }

    @Override
    public ReceptionEntity getReceptionById(UUID id) {
        return receptionRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Reception + " + id + "not found"));
    }

    @Override
    public void updateReceptionById(UUID id, ReceptionEntity receptionEntity) {
        ReceptionEntity reception = receptionRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Reception + " + id + "not found"));
        reception.setRoom(receptionEntity.getRoom());
        reception.setTime(receptionEntity.getTime());
        reception.setUserId(receptionEntity.getUserId());
        receptionRepository.save(reception);
    }
}
