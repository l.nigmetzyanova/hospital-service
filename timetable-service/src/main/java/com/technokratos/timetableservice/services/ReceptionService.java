package com.technokratos.timetableservice.services;

import com.technokratos.timetableservice.models.ReceptionEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ReceptionService {

    void createReception(ReceptionEntity receptionEntity);

    void deleteById(UUID id);

    List<ReceptionEntity> getAllReceptions();

    ReceptionEntity getReceptionById(UUID id);

    void updateReceptionById(UUID id, ReceptionEntity receptionEntity);

}
