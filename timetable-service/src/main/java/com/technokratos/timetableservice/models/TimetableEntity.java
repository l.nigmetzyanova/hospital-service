package com.technokratos.timetableservice.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "doctor_time_table")
public class TimetableEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "doctor_id")
    private UUID doctorId;

    @OneToOne
    @JoinColumn(name = "reception_id")
    private ReceptionEntity receptionId;
}
