package com.technokratos.timetableservice.util.exception.mapper;

import com.technokratos.timetableservice.dto.request.TimetableRequest;
import com.technokratos.timetableservice.dto.response.TimetableResponse;
import com.technokratos.timetableservice.feign.DoctorFeignService;
import com.technokratos.timetableservice.models.TimetableEntity;
import com.technokratos.timetableservice.services.impl.BaseReceptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TimetableMapper {

    private final BaseReceptionService receptionService;

    private final DoctorFeignService doctorFeignService;

    public TimetableEntity toEntity(TimetableRequest timetableRequest) {
        return TimetableEntity.builder()
                .receptionId(receptionService.getReceptionById(timetableRequest.getReceptionId()))
                .doctorId(timetableRequest.getDoctorId())
                .build();
    }

    public TimetableResponse toResponse(TimetableEntity timetable) {
        return TimetableResponse.builder()
                .time(timetable.getReceptionId().getTime())
                .room(timetable.getReceptionId().getRoom())
                .doctorName(doctorFeignService.getDoctorById(timetable.getDoctorId()).getName())
                .doctorQualification(doctorFeignService.getDoctorById(timetable.getDoctorId()).getQualification())
                .doctorSurname(doctorFeignService.getDoctorById(timetable.getDoctorId()).getSurname())
                .doctorSpecialization(doctorFeignService.getDoctorById(timetable.getDoctorId()).getSpecialization())
                .build();
    }

}
