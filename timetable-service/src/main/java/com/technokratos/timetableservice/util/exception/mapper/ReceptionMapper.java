package com.technokratos.timetableservice.util.exception.mapper;

import com.technokratos.timetableservice.dto.request.ReceptionRequest;
import com.technokratos.timetableservice.dto.response.ReceptionResponse;
import com.technokratos.timetableservice.models.ReceptionEntity;
import org.springframework.stereotype.Component;

@Component
public class ReceptionMapper {


    public ReceptionEntity toEntity(ReceptionRequest request) {
        return ReceptionEntity.builder()
                .userId(request.getUserId())
                .room(request.getRoom())
                .time(request.getTime())
                .build();
    }

    public ReceptionResponse toResponse(ReceptionEntity reception) {
        return ReceptionResponse.builder()
                .room(reception.getRoom())
                .time(reception.getTime())
                .build();
    }

}
