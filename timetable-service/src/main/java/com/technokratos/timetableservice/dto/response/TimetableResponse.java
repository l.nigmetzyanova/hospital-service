package com.technokratos.timetableservice.dto.response;


import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimetableResponse {

    private String doctorName;

    private String doctorSurname;

    private String doctorSpecialization;

    private String doctorQualification;

    private Timestamp time;

    private Integer room;

}
