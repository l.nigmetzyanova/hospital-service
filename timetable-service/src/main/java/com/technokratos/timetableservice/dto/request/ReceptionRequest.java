package com.technokratos.timetableservice.dto.request;

import lombok.*;

import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceptionRequest {

    private Integer room;

    private Timestamp time;

    private UUID userId;
}
