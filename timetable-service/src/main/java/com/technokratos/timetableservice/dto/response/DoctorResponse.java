package com.technokratos.timetableservice.dto.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DoctorResponse {

    private String name;

    private String surname;

    private String qualification;

    private String specialization;

}
