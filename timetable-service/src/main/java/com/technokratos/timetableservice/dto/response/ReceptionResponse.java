package com.technokratos.timetableservice.dto.response;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceptionResponse {

    private Integer room;

    private Timestamp time;


}
