package com.technokratos.timetableservice.dto.request;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimetableRequest {

    private UUID doctorId;

    private UUID receptionId;

}
