package com.technokratos.timetableservice.api;

import com.technokratos.timetableservice.dto.request.ReceptionRequest;
import com.technokratos.timetableservice.dto.response.ReceptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Tags(value = {
        @Tag(name = "diagnosis")
})
@RequestMapping("/reception")
public interface ReceptionApi {

    @Operation(summary = "Получение приемов")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Прием получен"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/all")
    List<ReceptionResponse> getAllReceptions();

    @Operation(summary = "Получение приема по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Прием получен"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    ReceptionResponse getReceptionById(@PathVariable UUID id);

    @Operation(summary = "Создание приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Прием сохранен"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/create")
    void createReception(@RequestBody ReceptionRequest receptionRequest);

    @Operation(summary = "Удаление приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Прием удален"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    void deleteReception(@PathVariable UUID id);

    @Operation(summary = "Обновление приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Прием обновлен"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    void updateReception(@PathVariable UUID id, @RequestBody ReceptionRequest receptionRequest);

}
