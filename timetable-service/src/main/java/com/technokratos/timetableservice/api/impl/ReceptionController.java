package com.technokratos.timetableservice.api.impl;


import com.technokratos.timetableservice.api.ReceptionApi;
import com.technokratos.timetableservice.dto.request.ReceptionRequest;
import com.technokratos.timetableservice.dto.response.ReceptionResponse;
import com.technokratos.timetableservice.services.impl.BaseReceptionService;
import com.technokratos.timetableservice.util.exception.mapper.ReceptionMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class ReceptionController implements ReceptionApi {

    private final BaseReceptionService receptionService;

    private final ReceptionMapper receptionMapper;

    @Override
    public List<ReceptionResponse> getAllReceptions() {
        return receptionService.getAllReceptions().stream()
                .map(receptionMapper :: toResponse).collect(Collectors.toList());
    }

    @Override
    public ReceptionResponse getReceptionById(@PathVariable UUID id) {
        return receptionMapper.toResponse(receptionService.getReceptionById(id));
    }

    @Override
    public void createReception(@RequestBody ReceptionRequest receptionRequest) {
        receptionService.createReception(receptionMapper.toEntity(receptionRequest));
    }

    @Override
    public void deleteReception(@PathVariable UUID id) {
        receptionService.deleteById(id);
    }

    @Override
    public void updateReception(@PathVariable UUID id, @RequestBody ReceptionRequest receptionRequest) {
        receptionService.updateReceptionById(id, receptionMapper.toEntity(receptionRequest));
    }

}
