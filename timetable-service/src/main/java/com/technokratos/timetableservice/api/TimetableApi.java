package com.technokratos.timetableservice.api;

import com.technokratos.timetableservice.dto.request.TimetableRequest;
import com.technokratos.timetableservice.dto.response.TimetableResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Tags(value = {
        @Tag(name = "timetable")
})
@RequestMapping("/timetable")
public interface TimetableApi {

    @Operation(summary = "Получение расписания")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Расписание получено"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/all")
    List<TimetableResponse> getAllTimetableRecords();

    @Operation(summary = "Получение записи расписания по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запись получена"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    TimetableResponse getTimetableRecordById(@PathVariable UUID id);

    @Operation(summary = "Создание записи расписания")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запись сохранена"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/create")
    void createTimetableRecord(@RequestBody TimetableRequest timetableRequest);

    @Operation(summary = "Удаление записи расписания")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запись удалена"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    void deleteTimetableRecord(@PathVariable UUID id);

    @Operation(summary = "Обновление записи расписания")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запись обновлена"),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере")

    })
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    void updateTimetableRecord(@PathVariable UUID id, @RequestBody TimetableRequest timetableRequest);

}
