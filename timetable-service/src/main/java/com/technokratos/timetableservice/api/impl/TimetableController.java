package com.technokratos.timetableservice.api.impl;


import com.technokratos.timetableservice.api.TimetableApi;
import com.technokratos.timetableservice.dto.request.TimetableRequest;
import com.technokratos.timetableservice.dto.response.TimetableResponse;
import com.technokratos.timetableservice.services.impl.BaseTimetableService;
import com.technokratos.timetableservice.util.exception.mapper.TimetableMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class TimetableController implements TimetableApi {

    private final BaseTimetableService timetableService;

    private final TimetableMapper timetableMapper;

    @Override
    public List<TimetableResponse> getAllTimetableRecords() {
        return timetableService.getAllTimetable().stream()
                .map(timetableMapper :: toResponse).collect(Collectors.toList());
    }

    @Override
    public TimetableResponse getTimetableRecordById(UUID id) {
        return timetableMapper.toResponse(timetableService.getTimetableRecordById(id));
    }

    @Override
    public void createTimetableRecord(TimetableRequest timetableRequest) {
        timetableService.createTimetableRecord(timetableMapper.toEntity(timetableRequest));
    }

    @Override
    public void deleteTimetableRecord(UUID id) {
        timetableService.deleteById(id);
    }

    @Override
    public void updateTimetableRecord(UUID id, TimetableRequest timetableRequest) {
        timetableService.updateTimetableRecordById(id, timetableMapper.toEntity(timetableRequest));
    }
}
