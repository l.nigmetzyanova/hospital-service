package com.example.auth.security.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString

public class CustomUserDetail extends User implements Serializable {
    private UUID id;
    private String login;

    public CustomUserDetail(UUID userId,
                            String username,
                            String password,
                            String role) {
        super(username,
              password,
              List.of(new SimpleGrantedAuthority(role)));
        this.id = userId;
        this.login = username;
    }
}
