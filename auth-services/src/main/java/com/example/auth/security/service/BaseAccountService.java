package com.example.auth.security.service;

import com.example.auth.entity.Account;
import com.example.auth.security.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseAccountService implements AccountService{

    private final AccountRepository accountRepository;

    @Override
    public Account getAccountById(UUID id) {
        return accountRepository.findById(id).orElseThrow();
    }

    @Override
    public Account getAccountByLogin(String login) {
        return accountRepository.getAccountByLogin(login);
    }


}
