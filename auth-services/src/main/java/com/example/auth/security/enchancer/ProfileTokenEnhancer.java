package com.example.auth.security.enchancer;

import com.example.auth.security.dto.CustomUserDetail;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;

@RequiredArgsConstructor
@Service
/*
    Тут можете положить в токен все что вам нужно по пользователю
 */
public class ProfileTokenEnhancer implements TokenEnhancer {

    private static final String ID = "ID";
    private static final String ROLE = "ROLE";
    private static final String EMAIL = "EMAIL";
    private static final String LOGIN = "LOGIN";

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Collection<GrantedAuthority> authorities = authentication.getAuthorities();
        String login = authentication.getName();

        if (authorities == null || login == null)
            return accessToken;

        HashMap<String, Object> additionalInformation = new HashMap<>();

        CustomUserDetail userDetail = (CustomUserDetail) authentication.getUserAuthentication().getPrincipal();
        additionalInformation.put(ID, userDetail.getId());
        additionalInformation.put(LOGIN, login);
        additionalInformation.put(EMAIL, userDetail.getEmail());
        String role = StringUtils.removeStart(authorities.stream().findFirst()
                .map(GrantedAuthority::getAuthority)
                .orElse(null), "ROLE_");
        additionalInformation.put(ROLE, role);
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);

        return accessToken;
    }
}
