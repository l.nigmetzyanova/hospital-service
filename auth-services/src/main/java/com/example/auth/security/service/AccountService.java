package com.example.auth.security.service;

import com.example.auth.entity.Account;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public interface AccountService {

    Account getAccountById(UUID id);

    Account getAccountByLogin(String login);
}
