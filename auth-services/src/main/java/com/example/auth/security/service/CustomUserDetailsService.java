package com.example.auth.security.service;

import com.example.auth.entity.Account;
import com.example.auth.security.dto.CustomUserDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final PasswordEncoder passwordEncoder;

    private final BaseAccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Account account = accountService.getAccountByLogin(login);

        if (account == null) {
            throw new UsernameNotFoundException(String.format("User with login %s not found", login));
        }
        return new CustomUserDetail(
                account.getId(),
                account.getLogin(),
                account.getPassword(),
                account.getRole()
        );
    }

}
